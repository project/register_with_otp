# Register With OTP

## Table of contents

- Introduction
- Features
- Requirements
- Installation
- Maintainers

## INTRODUCTION

This is a simple and Free module to add an extra protection in the
user Registration form (only for anonymous users) to let only valid users
create account and stop bot account creations.

## FEATURES

Whenever an user tries to create an account, they will have to verify their 
email id first, an OTP is sent to the provided email id, that is valid only for next
5 minutes, after that user will have to re verify their email id with a new OTP.
This idea prevents the bot account creations on the site.

## REQUIREMENTS

You need to have the smtp or any other mailing system configured on your site to make this module work properly, as all the OTPs will be sent via mail only.

## INSTALLATION

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## MAINTAINERS

Current maintainers:

- [Vishal Prasad](https://www.drupal.org/u/vishal-prasad)
