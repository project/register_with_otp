<?php

namespace Drupal\register_with_otp\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\RegisterForm;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Extends the core register form to introduce otp functionality.
 */
class RegisterWithOtp extends RegisterForm {

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $element = parent::actions($form, $form_state);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $session = $this->getRequest()->getSession();
    if ($this->currentUser()->isAnonymous()) {
      $form['#attached']['library'][] = 'register_with_otp/validator_styles';
      $form['account']['mail']['#prefix'] = '<div id="email-field">';
      $form['account']['mail']['#suffix'] = '</div>';
      if (!$session->get('otp_verified')) {
        $form['actions']['send_otp'] = [
          '#prefix' => '<div id="send-otp-btn">',
          '#suffix' => '</div>',
          '#type' => 'submit',
          '#value' => $this->t('Verify email'),
          '#attributes' => [
            'hidden' => !empty($session->get('otp')),
          ],
          '#ajax' => [
            'callback' => '::sendOtp',
            'event' => 'click',
            'progress' => [
              'type' => 'throbber',
            ],
          ],
        ];
      }
      $form['container'] = [
        '#type' => 'details',
        '#title' => $this->t('Verify OTP'),
        '#attributes' => [
          'id' => 'otp-form',
          'class' => ['form-card'],
          'hidden' => !empty($session->get('otp')) ? FALSE : TRUE,
        ],
        '#open' => TRUE,
      ];
      $form['container']['otp'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Enter your OTP'),
        '#required' => TRUE,
        '#pattern' => '[0-9]{5}',
        '#attributes' => [
          'id' => 'otp-field',
        ],
        '#maxlength' => 5,
        '#description' => $this->t('Please enter the numeric OTP sent to your email.'),
      ];

      $form['actions']['submit']['#attributes']['class'][] = 'create-account-btn';
      $form['actions']['submit']['#attributes']['hidden'] = !$session->get('otp');
    }
    return $form;
  }

  /**
   * Function to validate the email input and send otp to the email if correct.
   *
   * @param array $form
   *   The form build array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns Ajax Response.
   */
  public function sendOtp(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $session = $this->getRequest()->getSession();
    $email = trim($form_state->getValue('mail'));
    $message = '';
    $errors = $form_state->getErrors();

    // Checking if there is any form error related to email field.
    if (isset($errors['mail'])) {
      $message = $errors['mail'];
      $form['account']['mail']['#suffix'] = '<div class="email-error">' . $message . '</div>';
      $response->addCommand(new ReplaceCommand('#email-field', $form['account']['mail']));
    }
    // If there is no email error, further checking if the email is already
    // verified.
    elseif ($session->get('otp_verified') && $email === $session->get('email')) {
      $message = $this->t('@email is already verified', ['@email' => $email]);
      $form['account']['mail']['#suffix'] = '<div class="email-message">' . $message . '</div>';
      $response->addCommand(new ReplaceCommand('#email-field', $form['account']['mail']));
    }
    // If email is not verified, then sending the mail.
    else {
      $otp = random_int(10000, 90000);
      $email = $form_state->getValue('mail');
      $data['subject'] = 'OTP verification';
      $data['content'] = ['otp' => $otp];
      $timestamp = $this->getRequest()->server->get('REQUEST_TIME');

      $message = $this->sendOtpMail($otp, $data, $email, $timestamp, $session);

      // If mail is sent successfully.
      if ($message) {
        $form['account']['mail']['#suffix'] = '</div>';
        $form['container']['#attributes']['hidden'] = FALSE;
        $form['actions']['send_otp']['#attributes']['hidden'] = TRUE;
        $form['actions']['submit']['#attributes']['hidden'] = FALSE;
        $response->addCommand(new ReplaceCommand('.create-account-btn', $form['actions']['submit']));
        $response->addCommand(new ReplaceCommand('#send-otp-btn', $form['actions']['send_otp']));
        $response->addCommand(new ReplaceCommand('#otp-form', $form['container']));
        $response->addCommand(new ReplaceCommand('#email-field', $form['account']['mail']));
      }
      // If anything wrong happens while sending mail.
      else {
        $form['account']['mail']['#suffix'] = '<div class="email-error">' . $message . '</div>';
        $response->addCommand(new ReplaceCommand('#email-field', $form['account']['mail']));
      }
    }

    $this->messenger()->deleteAll();
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $session = $this->getRequest()->getSession();
    if ($this->currentUser()->isAnonymous()) {
      // If user tries to change the email after verification.
      if ($session->get('otp') && $session->get('email') != $form_state->getValue('mail')) {
        $errors = $form_state->getErrors();
        if (!isset($errors['mail'])) {
          $this->getRequest()->getSession()->set('email', $form_state->getValue('mail'));
          $form_state->setErrorByName('mail', $this->t("You can't change your email after OTP verification, kindly reverify"));
          $this->rebuildOtpField($form, $form_state);
        }
        return;
      }
      $current_time = $this->getRequest()->server->get('REQUEST_TIME');
      // Validating if the otp has been entered within five minutes.
      if (($current_time - $session->get('time')) <= 300) {
        if ($form_state->getValue('otp') != $session->get('otp')) {
          $form_state->setErrorByName('otp', $this->t("Invalid OTP Entered"));
          $session->set('otp_verified', FALSE);
          return;
        }
        else {
          $session->set('otp_verified', TRUE);
        }
      }
      // Asking user to reverify email when otp expires.
      else {
        $form_state->setErrorByName('otp', $this->t("The OTP is expired, kindly get a new otp to verify."));
        $this->rebuildOtpField($form, $form_state);

        $form_state->setRebuild(TRUE);
        return;
      }

      // If user tries to register without email verification.
      if (!$session->get('otp_verified')) {
        $form_state->setErrorByName('mail', $this->t('Please verify your email first.'));
        $session->set('otp_verified', FALSE);
        return;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->currentUser()->isAnonymous()) {
      $this->resetUserSession();
    }
    parent::submitForm($form, $form_state);
  }

  /**
   * Function to reset the otp data stored in Session.
   */
  protected function resetUserSession() {
    $session = $this->getRequest()->getSession();
    $session->remove('otp');
    $session->remove('time');
    $session->remove('email');
    $session->remove('otp_verified');
  }

  /**
   * Function to send the otp in mail.
   *
   * @param int $otp
   *   The otp to be mailed to the given email.
   * @param array $data
   *   The data array to be send in mail.
   * @param string $email
   *   The email to be send mail upon.
   * @param int $timestamp
   *   The current timestamp.
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   The session instance to be manipulated.
   *
   * @return string|bool
   *   Returns the success message or FALSE upon failure.
   */
  protected function sendOtpMail(int $otp, array $data, string $email, int $timestamp, SessionInterface $session) {
    $mailManager = \Drupal::service('plugin.manager.mail');
    try {
      $mailManager->mail("register_with_otp", "otp_mail_verification", $email, 'en', $data, NULL, TRUE);
      $session->set('otp', $otp);
      $session->set('time', $timestamp);
      $session->set('otp_verified', FALSE);
      $session->set('email', $email);
      $message = $this->t('An OTP has been sent to your email @email', ['@email' => $email]);
      $this->logger('register_with_otp')->notice(
        $this->t('A new OTP requested: @otp at time @time',
        [
          '@otp' => $otp,
          '@time' => $this->getRequest()->server->get('REQUEST_TIME'),
        ]
        )
      );

      return $message;
    }
    catch (\Exception $e) {
      $this->logger('register_with_otp')->notice($e->getMessage());
      return FALSE;
    }
  }

  /**
   * Function to rebuild the OTP field.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   */
  protected function rebuildOtpField(array &$form, FormStateInterface $form_state) {
    $this->resetUserSession();
    $form['actions']['submit']['#attributes']['hidden'] = TRUE;
    $form['actions']['send_otp']['#attributes']['hidden'] = FALSE;
    $form['container']['#attributes']['hidden'] = TRUE;

    $form_state->setRebuild(TRUE);
  }

}
